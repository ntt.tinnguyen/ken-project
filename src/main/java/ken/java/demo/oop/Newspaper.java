package ken.java.demo.oop;

import org.springframework.boot.autoconfigure.SpringBootApplication;

class Newspaper extends Document {
    private String releaseDate;

    public Newspaper() {
    }

    public String getReleaseDate() {
        return releaseDate;
    }

    public void setReleaseDate(String releaseDate) {
        this.releaseDate = releaseDate;
    }

    public String toString() {
        return super.toString()
                + "Release Date" + releaseDate;
    }
}
