package ken.java.demo.oop;

import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Library {

    static Scanner request = new Scanner(System.in);

    public static void main(String[] args) {
        List<Document> documents = new ArrayList<>();

        while (true) {
            System.out.println("Please enter the request:\n" +
                    "1. Add a new document.\n" +
                    "2. Delete a document.\n" +
                    "3. Show information about documents.\n" +
                    "4. Search for documents.\n" +
                    "5. Quit the program.\n ");
            int n = request.nextInt();
            request.nextLine();
            switch (n) {
                case 1:
                    addNewDocument(documents, request);
                    break;
                case 2:
                    deleteDocument(documents, request);
                    break;
                case 3:
                    showDocumentInfo(documents);
                    break;
                case 4:
                    findDocument(documents, request);
                    break;
                case 5:
                    return;
                default:
                    System.out.println("Invalid!");
            }
        }
    }

    public static void addNewDocument(List<Document> documents, Scanner in) {
        System.out.println("Please enter the type of document: \n" +
                "1. Book \n" +
                "2. Magazine \n" +
                "3. Newspaper \n");
        Document document;
        boolean inputDataSuccess;
        int userSelection = in.nextInt();
        in.nextLine();
        switch (userSelection) {
            case 1:
                document = new Book();
                inputDataSuccess = inputDocumentData(in, document, documents);
                if (!inputDataSuccess) {
                    System.out.println("The code has been existed!");
                    break;
                }
                System.out.println("4. Author: ");
                ((Book) document).setAuthor(in.nextLine());
                System.out.println("5. Page Quantity: ");
                ((Book) document).setPageQuantity(in.nextInt());
                in.nextLine();
                documents.add(document);
                break;
            case 2:
                document = new Magazine();
                inputDataSuccess = inputDocumentData(in, document, documents);
                if (!inputDataSuccess) {
                    System.out.println("The code has been existed!");
                    break;
                }
                System.out.println("4. Issue Number:");
                ((Magazine) document).setIssueNumber(in.nextInt());
                in.nextLine();
                System.out.println("5. Release Month: ");
                ((Magazine) document).setReleaseMonth(in.nextInt());
                in.nextLine();
                documents.add(document);
                break;
            case 3:
                document = new Newspaper();
                inputDataSuccess = inputDocumentData(in, document, documents);
                if (!inputDataSuccess) {
                    System.out.println("The code has been existed!");
                    break;
                }
                System.out.println("4. Release Date:");
                ((Newspaper) document).setReleaseDate(in.nextLine());
                documents.add(document);
                break;
            default:
                System.out.println("Invalid!");
        }
    }

    private static boolean inputDocumentData(Scanner in, Document document, List<Document> documents) {
        System.out.println("Please enter the document information: ");
        System.out.println("1. Code: ");
        document.setCode(in.nextLine());
        for (Document element : documents) {
            if (element.getCode().equals(document.getCode())) {
                return false;
            }
        }
        System.out.println("2. Publishing Company: ");
        document.setPublishingCompany(in.nextLine());
        System.out.println("3. Number of Releases: ");
        document.setReleasesQuantity(in.nextInt());
        in.nextLine();
        return true;
    }

    public static void deleteDocument(List<Document> documents, Scanner in) {
        System.out.println("Please enter the document code: ");
        String code = in.nextLine();
        Document document = null;
        for (Document element : documents) {
            if (element.getCode().equals(code)) {
                document = element;
                break;
            }
        }
        if (document != null) {
            documents.remove(document);
        }
    }

    public static void showDocumentInfo(List<Document> documents){
        int bookQuantity = 0;
        int magazineQuantity = 0;
        int newspaperQuantity = 0;
        for (Document element : documents) {
            if (element instanceof Book) {
                bookQuantity++;
            } else if (element instanceof Magazine) {
                magazineQuantity++;
            } else {
                newspaperQuantity++;
            }
        }
        System.out.println("Book Quantity: " + bookQuantity);
        System.out.println("Magazine Quantity: " + magazineQuantity);
        System.out.println("Newspaper Quantity: " + newspaperQuantity);
    }

    public static void findDocument(List<Document> documents, Scanner in) {
        System.out.println("Please enter the document code: ");
        String code = in.nextLine();
        Document document = null;
        for (Document element : documents) {
            if (element.getCode().equals(code)) {
                document = element;
                break;
            }
        }
        if (document != null) {
            System.out.println(document);
        }
        else {
            System.out.println("Can't find document with your code.");
        }
    }
}



