package ken.java.demo.oop;

import org.springframework.boot.autoconfigure.SpringBootApplication;

public class Magazine extends Document {
    private int issueNumber;
    private int releaseMonth;

    public Magazine() {
    }

    public int getIssueNumber() {
        return issueNumber;
    }

    public void setIssueNumber(int issueNumber) {
        this.issueNumber = issueNumber;
    }

    public int getReleaseMonth() {
        return releaseMonth;
    }

    public void setReleaseMonth(int releaseMonth) {
        this.releaseMonth = releaseMonth;
    }

    public String toString() {
        return super.toString()
                + "Issue Number" + issueNumber
                + "\nRelease Month" + releaseMonth;
    }
}
