package ken.java.demo.oop;

public class Document {
    protected String code;
    protected String publishingCompany;
    protected int releasesQuantity;

    public Document() {
    }

    private String doSomething() {
        return "";
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getPublishingCompany() {
        return publishingCompany;
    }

    public void setPublishingCompany(String publishingCompany) {
        this.publishingCompany = publishingCompany;
    }

    public int getReleasesQuantity() {
        return releasesQuantity;
    }

    public void setReleasesQuantity(int issueNumber) {
        this.releasesQuantity = releasesQuantity;
    }

    public String toString() {
        return "Code" + code
                + "\nPublishing Company" + publishingCompany
                + "\nReleases Quantity" + releasesQuantity;
    }
}
