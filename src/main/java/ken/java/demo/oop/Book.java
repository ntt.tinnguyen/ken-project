package ken.java.demo.oop;

public class Book extends Document {
    private String author;
    private int pageQuantity;

    public Book() {
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public int getPageQuantity() {
        return pageQuantity;
    }

    public void setPageQuantity(int pageQuantity) {
        this.pageQuantity = pageQuantity;
    }

    public String toString() {
        String currentCode = this.code;
        return super.toString()
                + "Author" + author
                + "\nPage Quantity" + pageQuantity;
    }
}
