package ken.java.demo.old;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import java.util.ArrayList;
import java.util.Scanner;

@SpringBootApplication
public class Bai62 {

    public static void main(String[] args) {
        Scanner a = new Scanner(System.in);
        System.out.print("Nhập số thứ nhất: ");
        int x = a.nextInt();
        System.out.print("Nhập số thứ hai: ");
        int y = a.nextInt();
        System.out.print("Nhập số thứ ba: ");
        int z = a.nextInt();
        System.out.println((Math.abs(x - y) >= 20 || Math.abs(y - z) >= 20 || Math.abs(z - x) >= 20));
    }
}
