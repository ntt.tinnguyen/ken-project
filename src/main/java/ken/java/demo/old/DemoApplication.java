package ken.java.demo.old;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.ArrayList;

@SpringBootApplication
public class DemoApplication {

    public static void main(String[] args) {
        ArrayList<Integer> list= new ArrayList<Integer>();
        list.add(1);
        list.add(1);
        int i = 2;
        int a;
        while(i<20){
            a = list.get(i-2) + list.get(i-1);
            list.add(a);
            i++;
        }
        System.out.println(list);
    }
}
