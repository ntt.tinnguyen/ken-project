package ken.java.demo.old;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import java.util.ArrayList;
import java.util.Scanner;

@SpringBootApplication
public class Bai66 {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.print("\nNhập số lượng số nguyên tố cho dãy: ");
        int a = in.nextInt();
        int sum = 0;
        int counterNumber = 0;
        int currentNumber = 2;
        ArrayList<Integer> daySoNguyenTo = new ArrayList<>();
        while (counterNumber < a) {
            if (timSoNguyenTo(currentNumber)) {
                daySoNguyenTo.add(currentNumber);
                counterNumber++;
                sum = sum + currentNumber;
            }
            currentNumber++;
        }
        System.out.println("\nDãy các số nguyên tố đầu tiên: " +daySoNguyenTo);
        System.out.println("\nTổng các số nguyên tố đầu tiên: " +sum);
    }

    public static boolean timSoNguyenTo(int n) {
        for (int i = 2; i <= n/2; i++) {
            if (n % i == 0) {
                return false;
            }
        }
        return true;
    }
}
