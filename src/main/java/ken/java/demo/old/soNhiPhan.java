package ken.java.demo.old;

import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.Scanner;

@SpringBootApplication
public class soNhiPhan {
    public static void main(String[] args) {
        int soThapPhan, n, i = 1, j;
        int soNhiPhan[] = new int[1000000000];
        Scanner scan = new Scanner(System.in);
        System.out.print("Nhập số thập phân : ");
        soThapPhan = scan.nextInt();
        n = soThapPhan;
        while (n != 0) {
            soNhiPhan[i++] = n % 2;
            n = n / 2;
        }
        System.out.print("Số nhị phân: ");
        for (j = i - 1; j > 0; j--) {
            System.out.print(soNhiPhan[j]);
        }
    }
}
