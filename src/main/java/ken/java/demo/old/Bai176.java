package ken.java.demo.old;

import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.Arrays;

@SpringBootApplication
public class Bai176 {
    public static void main(String[] args) {
        int[] numbers = {7, 2, 4, 1, 3, 5, 6, 8, 2, 10};
        System.out.println("Dãy số ban đầu: " + Arrays.toString(numbers));
        int[] result = partitionArray(numbers);
        System.out.println("Dãy số sau khi phân vùng: " + Arrays.toString(result));
    }

    public static int[] partitionArray(int[] nums) {
        int i = 0;
        int j = nums.length - 1;
        while (i < j) {
            while (nums[i] % 2 == 0) {
                i++;
            }
            while (nums[j] % 2 != 0) {
                j--;
            }
            if (i < j) {
                int temp = nums[i];
                nums[i] = nums[j];
                nums[j] = temp;
            }
        }
        return nums;
    }
}
