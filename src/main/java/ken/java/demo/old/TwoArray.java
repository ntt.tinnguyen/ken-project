package ken.java.demo.old;

import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.Arrays;

@SpringBootApplication
public class TwoArray {
    public static void main(String[] arg) {
        int[] array1 = {1, 4, 6, 3, 2, 9};
        int[] array2 = {56, 34, 25, 66, 2, 5, 3};
        int arrayLength = array1.length + array2.length;
        int[] combineArray = new int[arrayLength];

        for (int i = 0; i < array1.length; i++) {
            combineArray[i] = array1[i];
        }
        for (int j = array1.length; j < arrayLength; j++) {
            combineArray[j] = array2[j - array1.length];
        }

        for (int i = 0; i < combineArray.length; i++) {
            for (int j = i + 1; j < combineArray.length; j++) {
                if (combineArray[i] > combineArray[j]) {
                    int temp = combineArray[i];
                    combineArray[i] = combineArray[j];
                    combineArray[j] = temp;
                }
            }
        }
        System.out.println("\nArray 1: " + Arrays.toString(array1));
        System.out.println("\nArray: " + Arrays.toString(array2));
        System.out.println("\nNew Array: " + Arrays.toString(combineArray));
    }
}
