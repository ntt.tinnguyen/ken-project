package ken.java.demo.old;

import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

@SpringBootApplication
public class Bai165 {
    public static void main(String[] args) {
        int[] nums = {-2, 3, 4, -1, -3, 1, 2, -4, 0};
        System.out.println("\nOriginal array: " + Arrays.toString(nums));
        int[] result = partitionArray(nums);
        System.out.println("\nResult: " + Arrays.toString(result));
    }

    public static int[] partitionArray(int[] nums) {
        for (int i = 0; i < nums.length; i++) {
            for (int j = i + 1; j < nums.length; j++) {
                if (nums[i] > nums[j]) {
                    int temp = nums[i];
                    nums[i] = nums[j];
                    nums[j] = temp;
                }
            }
        }
        return nums;
    }
}
