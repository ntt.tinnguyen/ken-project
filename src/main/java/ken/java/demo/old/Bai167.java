package ken.java.demo.old;

import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.Arrays;

@SpringBootApplication
public class Bai167 {
    public static void main(String[] args) {
        int[] nums = {0, 3, 4, 0, 1, 2, 5, 0};
        System.out.println("\nOriginal array: " + Arrays.toString(nums));
        int[] result = zero(nums);
        System.out.println("\nResult: " + Arrays.toString(result));
    }

    public static int[] zero(int[] nums) {
        for (int i = 0; i < nums.length; i++) {
            for (int j = i + 1; j < nums.length; j++) {
                if (nums[i] == 0) {
                    int temp = nums[i];
                    nums[i] = nums[j];
                    nums[j] = temp;
                }
            }
        }
        return nums;
    }
}


