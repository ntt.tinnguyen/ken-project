package ken.java.demo.old;

import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.ArrayList;
import java.util.Scanner;

@SpringBootApplication
public class Bai49 {

    public static void main(String[] args) {
        Scanner a = new Scanner(System.in);
        System.out.print("Nhập một số bất kỳ: ");
        int n = a.nextInt();
        if (n % 2 == 0) {
            System.out.println(1);
        } else {
            System.out.println(0);
        }
    }
}