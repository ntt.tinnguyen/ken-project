package ken.java.demo.new_one;
import java.util.Scanner;

public class Practice1_1 {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);

        System.out.print("Input first number: ");
        int num1 = in.nextInt();

        System.out.print("Input second number: ");
        int num2 = in.nextInt();

        // TODO: should separate this code line into multiple variable declaring code lines.
        // int sum = num1 + num2;
        // sub = num1 - num2;
        // mul = num1 * num2;
        // div = num1 / num2;
        // rem = num1 % num2;
        // or you can pass directly the calculation in these lines
        System.out.println(num1 + " + " + num2 + " = " + (num1 + num2));
        System.out.println(num1 + " - " + num2 + " = " + (num1 - num2));
        System.out.println(num1 + " x " + num2 + " = " + (num1 * num2));
        System.out.println(num1 + " / " + num2 + " = " + (num1 / num2));
        System.out.println(num1 + " mod " + num2 + " = " + (num1 % num2));
    }

}
