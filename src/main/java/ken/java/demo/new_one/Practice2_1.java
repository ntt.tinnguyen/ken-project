package ken.java.demo.new_one;
import java.util.Arrays;
import java.util.Collections;

public class Practice2_1 {

    public static void main(String[] args) {

        Integer nums[] = {1, 4, 17, 7, 25, 3, 100};
        int k = 3;
        System.out.print("Original Array: ");
        System.out.println(Arrays.toString(nums));
        System.out.println();

        //bai 6
        System.out.print(k + " smallest elements of the array: ");
        Arrays.sort(nums);
        for (int i = 0; i < k; i++)
            System.out.print(nums[i] + " ");
        System.out.println("\n");

        //bai 5
        System.out.print(k + " largest elements of the array: ");
        Arrays.sort(nums, Collections.reverseOrder());
        for (int i = 0; i < k; i++)
            System.out.print(nums[i] + " ");
        System.out.println();

        //bai 11
        int sum = 0;
        for (int i = 0; i < nums.length; i++) {
            sum = sum + nums[i];
        }
        System.out.println("\nSum: " + sum);

        //bai 7
        double average = sum / nums.length;
        System.out.print("\nThe average of the array: " + average);
        System.out.print("\nThe numbers in the array that are greater than the average: ");
        for (int i = 0; i < nums.length; i++) {
            if (nums[i] > average) {
                System.out.print(nums[i] + " ");
            }
        }
        System.out.println();

        //bai 10
        for (int i = 0; i < nums.length; i++) {
            for (int j = i + 1; j < nums.length; j++) {
                if (nums[i] > nums[j]) {
                    int temp = nums[i];
                    nums[i] = nums[j];
                    nums[j] = temp;
                }
            }
        }
        System.out.println("\nResult: " + Arrays.toString(nums));

        //bai 12
        int sumOdd = 0;
        for (int i = 0; i < nums.length; i++) {
            if (nums[i] % 2 != 0) {
                sumOdd = sumOdd + nums[i];
            }
        }
        System.out.println("\nSum of odd elements: " + sumOdd);

    }

}
