package ken.java.demo.new_one;
import java.util.Scanner;

public class Practice1_4 {

    public static void main(String[] args) {
        double s = 6;
        System.out.println("Input the length of a side of the hexagon: " + s);
        double hexagonArea = (3 * Math.sqrt(3) * s * s) / 2;
        System.out.println("The area of the hexagon is: " + hexagonArea + "\n");
    }
}

