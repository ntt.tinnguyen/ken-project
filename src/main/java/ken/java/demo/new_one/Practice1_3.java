package ken.java.demo.new_one;
import java.util.Scanner;

public class Practice1_3 {

    public static void main(String[] args) {
        long binaryNumber = 10101010, decimalNumber = 0, i = 1, remainder;
        System.out.println("Binary number: " + binaryNumber);

        while (binaryNumber != 0)
        {
            remainder = binaryNumber % 10;
            decimalNumber = decimalNumber + remainder * i;
            i = i * 2;
            binaryNumber = binaryNumber / 10;
        }
        System.out.println("Decimal Number: " + decimalNumber);
    }
}