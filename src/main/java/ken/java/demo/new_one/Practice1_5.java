package ken.java.demo.new_one;

public class Practice1_5 {

    public static void main(String[] args) {
        int n = 7;
        System.out.println("Input the number of sides on the polygon: " + n);
        double s = 6;
        System.out.println("Input the length of one of the sides: " + s);
        double polygonArea = (n * (s * s)) / (4.0 * Math.tan((Math.PI / n)));
        System.out.println("The area is: " + polygonArea);
    }

}

