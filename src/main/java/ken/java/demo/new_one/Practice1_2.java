package ken.java.demo.new_one;
import java.util.Scanner;

public class Practice1_2 {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);

        System.out.print("Input first number: ");
        int num1 = in.nextInt();

        System.out.print("Input second number: ");
        int num2 = in.nextInt();

        System.out.println("Before swapping :  "+ num1 + ", "+ num2);
        int n = num1;
        num1 = num2;
        num2 = n;
        System.out.println("After swapping : "+ num1 + ", "+ num2);
    }

}
