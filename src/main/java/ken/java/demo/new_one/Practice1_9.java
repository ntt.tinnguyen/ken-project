package ken.java.demo.new_one;

public class Practice1_9 {

    public static void main(String[] args) {
        int a = 5, b = 10, c = 19, d = 13, e = 7, f = 7;
        compare(a, b);
        System.out.println();
        compare(c, d);
        System.out.println();
        compare(e, f);
    }

    private static void compare(int x, int y) {
        if (x == y) {
            System.out.print(0);
        } else {
            if ((x % 6) == (y % 6)) {
                if (x < y) {
                    System.out.print(x);
                } else {
                    System.out.print(y);
                }
            } else {
                if (x > y) {
                    System.out.print(x);
                } else {
                    System.out.print(y);
                }
            }
        }
    }

}

