package ken.java.demo.new_one;

public class HelloWorld {

    public static void main(String[] args) {
        // What is variable?
        // Declaring a variable.
        String helloWorld = "Hello" + " World";
        System.out.println(helloWorld);

        double a = 5;
        double b = 2;
        // true and true => true
        // true and false => false
        // true or true => true
        // true or false => true
        // false and false => false
        // false or false => false
        // false or true => true
        // false and true => true.
        // !true = false
        // !false = true
        
        if (a > 5) {
            System.out.println("a greater than 5");
        } else {
            System.out.println("a less than or equal 5");
        }
        
        while (a < 10) {
            System.out.println("a=" + a);
            a++;
        }
        
        for (int i = 0; i <= 10; i++) {
            if (i == 6) {
                break;
            }
            if (i % 2 != 0) {
                continue;
            }
            System.out.println("i=" + i);
        }

        System.out.println(a%b);
        // String. => data type.
        int myNumber = 10;
    }

}
